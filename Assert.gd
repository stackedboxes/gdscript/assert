#
# Assert
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2019-2021 Leandro Motta Barros
#

extends Node


# Asserts that v is true. Simple as that.
static func isTrue(v: bool) -> void:
	_assert(_isTrue(v),
		"isTrue failure: actual value (%s) is not true" % v)


# Asserts that v is false. Simple as that.
static func isFalse(v: bool) -> void:
	_assert(_isFalse(v),
		"isFalse failure: actual value (%s) is not false" % v)


# Asserts that v is null. Simple as that.
static func isNull(v) -> void:
	_assert(_isNull(v),
		"isNull failure: actual value (%s) is not null" % v)


# Asserts that expected and actual are equal. GDScript's standard == operator is
# used here, which means that numbers of different types can be compared among
# themselves (precision aside), but, say, numbers and strings cannot.
static func isEqual(expected, actual) -> void:
	_assert(_isEqual(expected, actual),
		"isEqual failure: actual value (%s) is not the expected (%s)" % [actual, expected])


# Asserts that a and b are not equal.
static func isNotEqual(a, b) -> void:
	_assert(_isNotEqual(a, b),
		"isNotEqual failure: %s is not different than %s" % [a, b])


# Asserts that the value a is greater than the value b.
static func isGT(a, b) -> void:
	_assert(_isGT(a, b),
		"isGT failure: %s is not greater than %s" % [a, b])


# Asserts that the value a is greater or equal than the value b.
static func isGE(a, b) -> void:
	_assert(_isGE(a, b),
		"isGE failure: %s is not greater or equal than %s" % [a, b])


# Asserts that the value a is less than the value b.
static func isLT(a, b) -> void:
	_assert(_isLT(a, b),
		"isLT failure: %s is not less than %s" % [a, b])


# Asserts that the value a is less or equal than the value b.
static func isLE(a, b) -> void:
	_assert(_isLE(a, b),
		"isLE failure: %s is not less or equal than %s" % [a, b])


# Asserts that expected and actual are close to each other within a tolerance of e.
static func isClose(expected: float, actual: float, e: float) -> void:
	_assert(_isClose(expected, actual, e),
		"isClose failure: actual (%s) and expected (%s) values are not within %.10f tolerance" % [actual, expected, e])


# Asserts that actual is small enough. Used to test if a value is near zero
# within an absolute difference of delta.
static func isSmall(actual: float, delta: float = 0.0) -> void:
	_assert(_isSmall(actual, delta),
		"isSmall failure: %s is not small enough (delta = %.10f)" % [actual, delta])


# Asserts that actual is in the [a, b] interval.
static func isBetweenCC(actual, a, b) -> void:
	_assert(_isBetweenCC(actual, a, b),
		"isBetweenCC failure: %s is not in the [%s, %s] interval" % [actual, a, b])


# Asserts that actual is in the (a, b) interval.
static func isBetweenOO(actual, a, b) -> void:
	_assert(_isBetweenOO(actual, a, b),
		"isBetweenOO failure: %s is not in the (%s, %s) interval" % [actual, a, b])


# Asserts that actual is in the [a, b) interval.
static func isBetweenCO(actual, a, b) -> void:
	_assert(_isBetweenCO(actual, a, b),
		"isBetweenCO failure: %s is not in the [%s, %s) interval" % [actual, a, b])


# Asserts that actual is in the (a, b] interval.
static func isBetweenOC(actual, a, b) -> void:
	_assert(_isBetweenOC(actual, a, b),
		"isBetweenOC failure: %s is not in the (%s, %s] interval" % [actual, a, b])


# Asserts that actual is a NaN (not-a-number).
static func isNaN(actual: float) -> void:
	_assert(_isNaN(actual),
		"isNaN: actual value (%s) is not a NaN" % actual)


# Checks whether v is true. Kind of ridiculous to have a function like this, but
# since I cannot test isTrue directly, I added this one anyway. (And then, kind
# of ridiculous to even test a function like this.)
static func _isTrue(v) -> bool:
	return v


# Checks whether v is false. Also ridiculous.
static func _isFalse(v) -> bool:
	return !v


# Checks whether v is null. Still ridiculous.
static func _isNull(v) -> bool:
	return v == null


# Checks whether u and v are equal.
static func _isEqual(u, v) -> bool:
	var typeOfU := typeof(u)
	var typeOfV := typeof(v)

	if typeOfU != typeOfV:
		var isNumericU := typeOfU == TYPE_INT || typeOfU == TYPE_REAL
		var isNumericV := typeOfV == TYPE_INT || typeOfV == TYPE_REAL
		if !isNumericU || !isNumericV:
			return false

	match typeOfU:
		TYPE_ARRAY:
			if len(u) != len(v):
				return false
			for i in range(0, len(u)):
				if !_isEqual(u[i], v[i]):
					return false
			return true

		_:
			return u == v


# Checks whether u is not equal to v.
static func _isNotEqual(u, v) -> bool:
	return !_isEqual(u, v)


# Checks whether u is greater than v.
static func _isGT(u, v) -> bool:
	return u > v


# Checks whether u is greater or equal than v.
static func _isGE(u, v) -> bool:
	return u >= v


# Checks whether u is less than v.
static func _isLT(u, v) -> bool:
	return u < v


# Checks whether u is less or equal than v.
static func _isLE(u, v) -> bool:
	return u <= v


# Checks whether u and v are close to each other within a tolerance of e.
#
# This implements the same "very close with tolerance e" algorithm used by Boost
# Test (http://www.boost.org/doc/libs/1_56_0/libs/test/doc/html/utf/testing-tools/floating_point_comparison.html, Boost Test),
# which works nicely with both very large and very small numbers.
static func _isClose(u: float, v: float, e: float) -> bool:
	var d := abs(u - v)
	return d == 0 || (d / abs(u) <= e && d / abs(v) <= e)


# Checks whether v is close enough to zero. The algorithm used by _isClose is
# not appropriate for testing if a value is (approximately) zero. Use this
# instead.
static func _isSmall(v: float, delta: float = 0.0) -> bool:
	return abs(v) <= delta


# Checks whether v is in the [a, b] interval.
static func _isBetweenCC(v, a, b) -> bool:
	return v >= a && v <= b


# Checks whether v is in the (a, b) interval.
static func _isBetweenOO(v, a, b) -> bool:
	return v > a && v < b


# Checks whether v is in the [a, b) interval.
static func _isBetweenCO(v, a, b) -> bool:
	return v >= a && v < b


# Checks whether v is in the (a, b] interval.
static func _isBetweenOC(v, a, b) -> bool:
	return v > a && v <= b


# Checks whether v is a NaN.
static func _isNaN(v) -> bool:
	return is_nan(v)


# Asserts that cond is true. If it fails, it show a nice-enough message,
# including the message passed as errMsg.
static func _assert(cond: bool, errMsg: String) -> void:
	if !cond:
		print("ASSERTION FAILED!")
		print(errMsg)
		print_stack()
		assert(false)
