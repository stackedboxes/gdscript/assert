# Assert

*Part of [StackedBoxes' GDScript Hodgepodge](https://gitlab.com/stackedboxes/gdscript/)*

Simple asserts for testing. If you are serious about unit testing in Godot, take a look at [Gut](https://github.com/bitwes/Gut).
