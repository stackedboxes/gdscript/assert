#
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2019 Leandro Motta Barros
#

.SILENT: help test

help:
	echo "Run 'make test' to run the unit tests."
	echo
	echo "If you don't see any 'Assertion failed' error, it means tests passed."

test:
	headless-godot --debug --script Test.gd
