#
# Assert
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2019-2021 Leandro Motta Barros
#

extends SceneTree

var Assert

func _init():
	Assert = preload("res://Assert.gd")

	TestIsTrue(true, true)
	TestIsTrue(false, false)

	TestIsFalse(false, true)
	TestIsFalse(true, false)

	TestIsNull(null, true)
	TestIsNull("null", false)
	TestIsNull(171, false)
	TestIsNull([], false)

	TestIsEqual(1, 1, true)
	TestIsEqual(1, -1, false)
	TestIsEqual(1, 1.0, true)
	TestIsEqual(1, 1.000001, false)
	TestIsEqual(10, "ten", false)
	TestIsEqual("ten", 10, false)
	TestIsEqual("cake", "cake", true)
	TestIsEqual("pie", "cake", false)
	TestIsEqual(true, false, false)
	TestIsEqual(true, true, true)
	TestIsEqual([], [], true)
	TestIsEqual([1], [1, 2], false)
	TestIsEqual([1, 2, 3], [1, 2, 3], true)
	TestIsEqual([1, 2, 3], [3, 2, 1], false)
	TestIsEqual(["a", "b", "c"], ["a", "b", "c"], true)
	TestIsEqual(["a", "b", "c"], ["A", "B", "C"], false)
	TestIsEqual([1, 2], 12, false)
	TestIsEqual(12, [1, 2], false)

	TestIsNotEqual(1, 1, false)
	TestIsNotEqual(1, -1, true)
	TestIsNotEqual(1, 1.0, false)
	TestIsNotEqual(1, 1.000001, true)
	TestIsNotEqual("cake", "cake", false)
	TestIsNotEqual("pie", "cake", true)
	TestIsNotEqual(true, false, true)
	TestIsNotEqual(true, true, false)
	TestIsNotEqual([], [], false)
	TestIsNotEqual([1, 2], [1, 2], false)
	TestIsNotEqual([1, "2"], [1, 2], true)
	TestIsNotEqual(1, [1, 2], true)
	TestIsNotEqual([1, 2], 1, true)

	TestIsGT(1, 0, true)
	TestIsGT(-2.1, -10.2, true)
	TestIsGT(4.0, 4.0, false)
	TestIsGT(-1.0, 1.0, false)

	TestIsGE(1, 0, true)
	TestIsGE(-2.1, -10.2, true)
	TestIsGE(4.0, 4.0, true)
	TestIsGE(-1.0, 1.0, false)

	TestIsLT(1, 0, false)
	TestIsLT(-2.1, -10.2, false)
	TestIsLT(4.0, 4.0, false)
	TestIsLT(-1.0, 1.0, true)

	TestIsLE(1, 0, false)
	TestIsLE(-2.1, -10.2, false)
	TestIsLE(4.0, 4.0, true)
	TestIsLE(-1.0, 1.0, true)

	TestIsClose(0.001, 0.001001, 0.001, true)
	TestIsClose(0.001001, 0.001, 0.001, true)
	TestIsClose(0.001, 0.001001, 0.0001, false)
	TestIsClose(0.001001, 0.001, 0.0001, false)
	TestIsClose(10.0e4, 10.01e4, 0.001, true)
	TestIsClose(10.01e4, 10.0e4, 0.001, true)
	TestIsClose(10.0e4, 10.01e4, 0.0001, false)
	TestIsClose(10.01e4, 10.0e4, 0.0001, false)
	TestIsClose(0.0, 0.0, 1e-10, true)
	TestIsClose(0.2, 0.2, 1e-10, true)

	TestIsSmall(0.0000001, 1e-7, true)
	TestIsSmall(-0.0000001, 1e-7, true)
	TestIsSmall(0.000001, 1e-7, false)
	TestIsSmall(-0.000001, 1e-7, false)
	TestIsSmall(0.0, 1e-7, true)
	TestIsSmall(0.0, 0.0, true)

	TestIsBetweenCC(2, 1, 3, true)
	TestIsBetweenCC(2.2, 1.2, 3.3, true)
	TestIsBetweenCC(-1, -2, 5, true)
	TestIsBetweenCC(-3, -5, -2, true)
	TestIsBetweenCC(5, 5, 5, true)
	TestIsBetweenCC(5, 0, 5, true)
	TestIsBetweenCC(5, 5, 15, true)
	TestIsBetweenCC(5, 1, 2, false)
	TestIsBetweenCC(1.2, 1.201, 1.3, false)

	TestIsBetweenOO(2, 1, 3, true)
	TestIsBetweenOO(2.2, 1.2, 3.3, true)
	TestIsBetweenOO(-1, -2, 5, true)
	TestIsBetweenOO(-3, -5, -2, true)
	TestIsBetweenOO(5, 5, 5, false)  # empty interval
	TestIsBetweenOO(5, 0, 5, false)
	TestIsBetweenOO(5, 5, 15, false)
	TestIsBetweenOO(5, 1, 2, false)
	TestIsBetweenOO(1.2, 1.201, 1.3, false)

	TestIsBetweenCO(2, 1, 3, true)
	TestIsBetweenCO(2.2, 1.2, 3.3, true)
	TestIsBetweenCO(-1, -2, 5, true)
	TestIsBetweenCO(-3, -5, -2, true)
	TestIsBetweenCO(5, 5, 5, false) # empty interval
	TestIsBetweenCO(5, 0, 5, false)
	TestIsBetweenCO(5, 5, 15, true)
	TestIsBetweenCO(5, 1, 2, false)
	TestIsBetweenCO(1.2, 1.201, 1.3, false)

	TestIsBetweenOC(2, 1, 3, true)
	TestIsBetweenOC(2.2, 1.2, 3.3, true)
	TestIsBetweenOC(-1, -2, 5, true)
	TestIsBetweenOC(-3, -5, -2, true)
	TestIsBetweenOC(5, 5, 5, false) # empty interval
	TestIsBetweenOC(5, 0, 5, true)
	TestIsBetweenOC(5, 5, 15, false)
	TestIsBetweenOC(5, 1, 2, false)
	TestIsBetweenOC(1.2, 1.201, 1.3, false)

	TestIsNaN(NAN, true)
	TestIsNaN(0.0, false)

	quit()


# Tests _isTrue
func TestIsTrue(v, expected: bool) -> void:
	assert(Assert._isTrue(v) == expected)


# Tests _isFalse
func TestIsFalse(v, expected: bool) -> void:
	assert(Assert._isFalse(v) == expected)


# Tests _isNull
func TestIsNull(v, expected: bool) -> void:
	assert(Assert._isNull(v) == expected)


# Tests _isEqual
func TestIsEqual(u, v, expected: bool) -> void:
	assert(Assert._isEqual(u, v) == expected)


# Tests _isNotEqual
func TestIsNotEqual(u, v, expected: bool) -> void:
	assert(Assert._isNotEqual(u, v) == expected)


# Tests _isGT
func TestIsGT(u, v, expected: bool) -> void:
	assert(Assert._isGT(u, v) == expected)


# Tests _isGE
func TestIsGE(u, v, expected: bool) -> void:
	assert(Assert._isGE(u, v) == expected)


# Tests _isLT
func TestIsLT(u, v, expected: bool) -> void:
	assert(Assert._isLT(u, v) == expected)


# Tests _isLE
func TestIsLE(u, v, expected: bool) -> void:
	assert(Assert._isLE(u, v) == expected)


# Tests _isClose
func TestIsClose(u: float, v: float, e: float, expected: bool) -> void:
	assert(Assert._isClose(u, v, e) == expected)


# Tests _isSmall
func TestIsSmall(v: float, delta: float, expected: bool) -> void:
	assert(Assert._isSmall(v, delta) == expected)


# Tests _isBetweenCC
func TestIsBetweenCC(v: float, a: float, b: float, expected: bool) -> void:
	assert(Assert._isBetweenCC(v, a, b) == expected)


# Tests _isBetweenOO
func TestIsBetweenOO(v: float, a: float, b: float, expected: bool) -> void:
	assert(Assert._isBetweenOO(v, a, b) == expected)


# Tests _isBetweenOC
func TestIsBetweenOC(v: float, a: float, b: float, expected: bool) -> void:
	assert(Assert._isBetweenOC(v, a, b) == expected)


# Tests _isBetweenCO
func TestIsBetweenCO(v: float, a: float, b: float, expected: bool) -> void:
	assert(Assert._isBetweenCO(v, a, b) == expected)


# Tests _isNaN
func TestIsNaN(v: float, expected: bool) -> void:
	assert(Assert._isNaN(v) == expected)
